import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {RedSocial} from '../models';
import {RedSocialRepository} from '../repositories';

export class RedSocialController {
  constructor(
    @repository(RedSocialRepository)
    public redSocialRepository : RedSocialRepository,
  ) {}

  @post('/red-socials', {
    responses: {
      '200': {
        description: 'RedSocial model instance',
        content: {'application/json': {schema: getModelSchemaRef(RedSocial)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RedSocial, {
            title: 'NewRedSocial',
            exclude: ['id'],
          }),
        },
      },
    })
    redSocial: Omit<RedSocial, 'id'>,
  ): Promise<RedSocial> {
    return this.redSocialRepository.create(redSocial);
  }

  @get('/red-socials/count', {
    responses: {
      '200': {
        description: 'RedSocial model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(RedSocial) where?: Where<RedSocial>,
  ): Promise<Count> {
    return this.redSocialRepository.count(where);
  }

  @get('/red-socials', {
    responses: {
      '200': {
        description: 'Array of RedSocial model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(RedSocial, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(RedSocial) filter?: Filter<RedSocial>,
  ): Promise<RedSocial[]> {
    return this.redSocialRepository.find(filter);
  }

  @patch('/red-socials', {
    responses: {
      '200': {
        description: 'RedSocial PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RedSocial, {partial: true}),
        },
      },
    })
    redSocial: RedSocial,
    @param.where(RedSocial) where?: Where<RedSocial>,
  ): Promise<Count> {
    return this.redSocialRepository.updateAll(redSocial, where);
  }

  @get('/red-socials/{id}', {
    responses: {
      '200': {
        description: 'RedSocial model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(RedSocial, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(RedSocial, {exclude: 'where'}) filter?: FilterExcludingWhere<RedSocial>
  ): Promise<RedSocial> {
    return this.redSocialRepository.findById(id, filter);
  }

  @patch('/red-socials/{id}', {
    responses: {
      '204': {
        description: 'RedSocial PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(RedSocial, {partial: true}),
        },
      },
    })
    redSocial: RedSocial,
  ): Promise<void> {
    await this.redSocialRepository.updateById(id, redSocial);
  }

  @put('/red-socials/{id}', {
    responses: {
      '204': {
        description: 'RedSocial PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() redSocial: RedSocial,
  ): Promise<void> {
    await this.redSocialRepository.replaceById(id, redSocial);
  }

  @del('/red-socials/{id}', {
    responses: {
      '204': {
        description: 'RedSocial DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.redSocialRepository.deleteById(id);
  }
}
