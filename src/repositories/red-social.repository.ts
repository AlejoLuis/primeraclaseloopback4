import {DefaultCrudRepository} from '@loopback/repository';
import {RedSocial, RedSocialRelations} from '../models';
import {DbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class RedSocialRepository extends DefaultCrudRepository<
  RedSocial,
  typeof RedSocial.prototype.id,
  RedSocialRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(RedSocial, dataSource);
  }
}
