import {Entity, model, property} from '@loopback/repository';

@model()
export class RedSocial extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  Instagram: string;

  @property({
    type: 'string',
    required: true,
  })
  Facebook: string;

  @property({
    type: 'string',
    required: true,
  })
  Twitter: string;


  constructor(data?: Partial<RedSocial>) {
    super(data);
  }
}

export interface RedSocialRelations {
  // describe navigational properties here
}

export type RedSocialWithRelations = RedSocial & RedSocialRelations;
