import {Entity, model, property} from '@loopback/repository';

@model()
export class Publication extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  Description: string;

  @property({
    type: 'any',
  })
  Image?: any;

  @property({
    type: 'string',
    required: true,
  })
  typeOfSocialNetwork: string;


  constructor(data?: Partial<Publication>) {
    super(data);
  }
}

export interface PublicationRelations {
  // describe navigational properties here
}

export type PublicationWithRelations = Publication & PublicationRelations;
